clc;
clear;
close all;
H=0;
U=0;
Feat=zeros(400,100000);
Label=cell(1,100000);
for fi=61:80
    Fn = ['C:\Users\saheb\Desktop\Science\DSP MATLAB Project\DataSets\1\pic (',int2str(fi),').jpg'];
    if exist(Fn)
        Pic= imread(Fn);
        Pic=FindPlate(Pic);
        [Ch,K]=ExTraction(Pic);
        if K>0
            Feat(:,H+1:H+K) = Ch;
            Ch2 = reshape(Ch,[20,K*20]);
            figure(1);
            imshow(Ch2,[]);
            imwrite(Ch2,['C:\Users\saheb\Desktop\Science\DSP MATLAB Project\Features\1\(',int2str(fi),').tif']);
            Lbl = input(['Enter ',int2str(K),' labels : '],'s')
            L = cell((numel(Lbl)+1)/2  ,1);
            for i = 1:2:numel(Lbl)
                L{(i+1)/2} = (Lbl(i));
            end
            Label(H+1:H+K) = L;
            H=H+K;
        end
        
    end
end
Feat=Feat(:,1:H);
for i = 1:H
    if (i==1 && Label{i}=='@')
        U=U+1;
        Feat(:,i)=[];
        Label{i}=[];
    elseif (Label{i}=='@')
        U=U+1;
        Feat(:,i-U)=[];
        Label{i}=[];
    end
end
H=H-U;
Label(cellfun(@isempty,Label))=[];
Trgt=zeros(30,H);
for i=1:H
    switch Label{i}
        case '1'
            Trgt(1,i)=1;
        case '2'
            Trgt(2,i)=1;
        case '3'
            Trgt(3,i)=1;
        case '4'
            Trgt(4,i)=1;
        case '5'
            Trgt(5,i)=1;
        case '6'
            Trgt(6,i)=1;
        case '7'
            Trgt(7,i)=1;
        case '8'
            Trgt(8,i)=1;
        case '9'
            Trgt(9,i)=1;
        case '0'
            Trgt(10,i)=1;
        case 'h'
            Trgt(11,i)=1;
        case 'f'
            Trgt(12,i)=1;
        case '\'
            Trgt(13,i)=1;
        case 'j'
            Trgt(14,i)=1;
        case 'e'
            Trgt(15,i)=1;
        case 'p'
            Trgt(16,i)=1;
        case 'n'
            Trgt(17,i)=1;
        case 'v'
            Trgt(18,i)=1;
        case 's'
            Trgt(19,i)=1;
        case 'w'
            Trgt(20,i)=1;
        case 'x'
            Trgt(21,i)=1;
        case 'u'
            Trgt(22,i)=1;
        case 't'
            Trgt(23,i)=1;
        case 'r'
            Trgt(24,i)=1;
        case ';'
            Trgt(25,i)=1;
        case 'g'
            Trgt(26,i)=1;
        case 'l'
            Trgt(27,i)=1;
        case 'k'
            Trgt(28,i)=1;
        case ','
            Trgt(29,i)=1;
        case 'd'
            Trgt(30,i)=1;
    end
end
 save FinalFeat Feat Trgt