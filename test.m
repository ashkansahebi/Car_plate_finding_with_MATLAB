clc;
clear;
close all;
for fi=56
    Fn = ['DataSets\1\pic (',int2str(fi),').jpg'];
    if exist(Fn,'file')
        Pic= imread(Fn);
        Pic=FindPlate(Pic);
        [Ch,K]=ExTraction(Pic);
        Ch2 = reshape(Ch,[20,K*20]);
        figure(1); subplot(1,2,2); imshow(Ch2)
        fi;
    end
end