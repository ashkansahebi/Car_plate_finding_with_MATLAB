clc;
clear;
close all;
load PatternNetwork;
for fi=2
    Fn = ['C:\Users\saheb\Desktop\Science\DSP MATLAB Project\pictures\pic (',int2str(fi),').jpg'];
    if exist(Fn)
      Pic= imread(Fn);
    end
end
subplot(1,2,1)
imshow(Pic);
Pll2=FindPlate(Pic);
subplot(1,2,2)
imshow(Pll2);
[Ch,K]=ExTraction(Pll2);
 Y = Net(Ch);
 Mainpelak=max(Y);
 Numpelak = zeros(1,K);
 for i=1:K
 BB=Mainpelak(1,i);
 [X1,Y1]=find(BB==Y);
 Numpelak(1,i)=X1;
 end