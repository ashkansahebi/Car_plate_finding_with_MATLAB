function [Ch,K]=ExTraction(Pll2)
Ch=zeros(400,1000);
K=0;
P=80;
D=2;
for i=1:max(Pll2(:))
    Im5 = (Pll2==i);
    if sum(Im5(:))>1000 && sum(Im5(:))<5000
        [Xx,Yy] = find(Im5>0);
        Im5 = Im5(min(Xx):max(Xx),min(Yy):max(Yy));
        K=K+1;
        [T,V] = size (Im5);
        if T>V
            Tmp = zeros (T , T-V);
            Im5 = [Im5 , Tmp];
            Im5 = imresize(Im5, [P P]);
            for e=1:D
                Im5 = dwt2 (Im5 , 'haar');
            end
            Im5 = Im5(:);
        else
            Tmp = zeros (V-T , V);
            Im5 = [Im5 ; Tmp];
            Im5 = imresize (Im5, [P P]);
            for e=1:D
                Im5 = dwt2 (Im5 , 'haar');
            end
            Im5 = Im5(:);
        end
          Ch(:,K)= Im5;
    end  
end
    Ch=Ch(:,1:K);
end
